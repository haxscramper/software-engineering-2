Структура некоторых AST элементов

```text
nnkProcDef(
  nnkIdent("procName"),
  nnkFormalParams(
    nkVarDecl(
      nnkIdent("x"),
      nnkIdent("int"),
    ),
  ),
  nk.Ident("ReturnType")
  nkBlock(... ...)
)
```
