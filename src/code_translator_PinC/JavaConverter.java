package code_translator_PinC;
import static code_translator_PinC.StringUtils.*;

import code_translator_PinC.*;
import java.util.stream.*;

public class JavaConverter extends CLikeConverter
{
    public String convert(NodeTree tree) {
        tree.getKind().expectKind(nk.Maincode);
        String res = "";
        for (NodeTree node : tree.getSubnodes()) {
            switch (node.getKind()) {
                case Block: {
                    res += fmt(
                        "  public static void Main(String[] args) {\n%s\n  }",
                        conv(node, 2));
                    break;
                }
                default: {
                    res += conv(node, 1);
                }
            }
            res += "\n";
        }

        return fmt("class Main {\n%s\n}", res);
    }

    protected String convertTypeNode(NodeTree tree) {
        return convertIdent(tree, 0);
    }

    public String convertProcDecl(NodeTree tree, int level) {
        return fmt("  public static %s %s(%s) {\n",
                   conv(tree.get(2), level + 1), // return type
                   conv(tree.get(0), level + 1), // function identifier
                   conv(tree.get(1), level + 1)  // parameter list
                   )
            + indent(conv(tree.get(3), level + 1), 2 * (level + 2)) + ";\n"
            +                                   // variable declaration
            conv(tree.get(4), level + 1) + "\n" // implementation body
            + "  }\n";
    }
}
