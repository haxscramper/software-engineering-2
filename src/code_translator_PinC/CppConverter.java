package code_translator_PinC;

import static code_translator_PinC.StringUtils.*;

import code_translator_PinC.*;
import java.util.stream.*;

public class CppConverter extends CLikeConverter
{
    public String convert(NodeTree tree) {
        tree.getKind().expectKind(nk.Maincode);
        String res = "";
        // System.out.println(tree.treeRepr(0));
        for (NodeTree node : tree.getSubnodes()) {
            res += "//  ----- \n";
            switch (node.getKind()) {
                case Block: {
                    res += fmt("int main() {\n%s\n}", conv(node, 1));
                    break;
                }
                default: {
                    res += conv(node, 0);
                }
            }
        }

        return res;
    }


    protected String convertTypeNode(NodeTree tree) {
        return convertIdent(tree, 0);
    }

    protected String convertProcDecl(NodeTree tree, int level) {
        return fmt("%s %s(%s) {\n",
                   conv(tree.get(2), level + 1), // return type
                   conv(tree.get(0), level + 1), // function identifier
                   conv(tree.get(1), level + 1)  // parameter list
                   )
            + indent(conv(tree.get(3), level + 1), 2 * (level + 2)) + ";\n"
            +                                   // variable declaration
            conv(tree.get(4), level + 1) + "\n" // implementation body
            + "}\n";
    }
}
