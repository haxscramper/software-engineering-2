package code_translator_PinC;

import static code_translator_PinC.StringUtils.*;

import code_translator_PinC.*;
import java.util.ArrayList;
import java.util.stream.*;

public abstract class ConverterBase
{
    protected abstract String convert(NodeTree tree);

    protected abstract String convertTypeNode(NodeTree tree);

    // clang-format off
    // protected abstract String convertBlock      (NodeTree tree, int level);

    protected abstract String convertProcDecl   (NodeTree tree, int level);
    protected abstract String convertFuncDecl   (NodeTree tree, int level);
    protected abstract String convertParamList  (NodeTree tree, int level);
    protected abstract String convertVarSection (NodeTree tree, int level);

    protected abstract String convertFor        (NodeTree tree, int level);
    protected abstract String convertWhile      (NodeTree tree, int level);
    protected abstract String convertDo         (NodeTree tree, int level);
    protected abstract String convertRepeat     (NodeTree tree, int level);
    protected abstract String convertIf         (NodeTree tree, int level);
    protected abstract String convertElseIf     (NodeTree tree, int level);
    protected abstract String convertElse       (NodeTree tree, int level);
    protected abstract String convertAsgn       (NodeTree tree, int level);
    protected abstract String convertCall       (NodeTree tree, int level);
    protected abstract String convertVarDecl    (NodeTree tree, int level);
    protected abstract String convertRetStmt    (NodeTree tree, int level);

    // clang-format on
    public String convertBlock(NodeTree tree, int level) {
        String pref = repeat(" ", level * 2);
        return conv(tree.getSubnodes(), level + 1, "\n");
    }

    public String convertExpr(NodeTree tree, int level) {
        return conv(tree.getSubnodes(), level + 1, ", ");
    }

    public String convertInfix(NodeTree tree, int level) {
        return fmt(
            "%s %s %s",
            conv(tree.get(1), level + 1),
            conv(tree.get(0), level + 1),
            conv(tree.get(2), level + 1));
    }


    public String convertPrefix(NodeTree tree, int level) {
        return fmt(
            "%s %s",
            conv(tree.get(0), level + 1),
            conv(tree.get(1), level + 1));
    }

    public String convertPostfix(NodeTree tree, int level) {
        return fmt(
            "%s %s",
            conv(tree.get(1), level + 1),
            conv(tree.get(0), level + 1));
    }


    public String convertIdent(NodeTree tree, int level) {
        return tree.getStr();
    }
    public String convertOpIdent(NodeTree tree, int level) {
        return tree.getStr();
    }


    public String convertEmpty(NodeTree tree, int level) { return ""; }

    public String convertPar(NodeTree tree, int level) {
        return fmt(
            "(%s)",
            String.join(
                ", ",
                tree.getSubnodes()
                    .stream()
                    .map(subn -> conv(subn, level + 1))
                    .collect(Collectors.toList())

                    ));
    }

    public String convertInt(NodeTree tree, int level) {
        return fmt("%d", tree.getInt());
    }

    public String convertReal(NodeTree tree, int level) {
        return fmt("%f", tree.getReal());
    }

    public String convertStr(NodeTree tree, int level) {
        return fmt("%s", tree.getStr());
    }


    protected String conv(NodeTree tree, int level) {
        switch (tree.getKind()) {
            // clang-format off
            case Block:      return convertBlock      (tree, level);
            case ProcDecl:   return convertProcDecl   (tree, level);
            case FuncDecl:   return convertFuncDecl   (tree, level);
            case ParamList:  return convertParamList  (tree, level);
            case VarSection: return convertVarSection (tree, level);
            case Empty:      return convertEmpty      (tree, level);
            case Par:        return convertPar        (tree, level);

            case Int:        return convertInt        (tree, level);
            case Real:       return convertReal       (tree, level);
            case Str:        return convertStr        (tree, level);
            case For:        return convertFor        (tree, level);
            case While:      return convertWhile      (tree, level);
            case Do:         return convertDo         (tree, level);

            case Repeat:     return convertRepeat     (tree, level);
            case If:         return convertIf         (tree, level);
            case ElseIf:     return convertElseIf     (tree, level);
            case Else:       return convertElse       (tree, level);
            case Asgn:       return convertAsgn       (tree, level);
            case Call:       return convertCall       (tree, level);
            case VarDecl:    return convertVarDecl    (tree, level);
            case Expr:       return convertExpr       (tree, level);
            case RetStmt:    return convertRetStmt    (tree, level);

            case Ident:      return convertIdent      (tree, level);
            case OpIdent:    return convertOpIdent    (tree, level);
            case Infix:      return convertInfix      (tree, level);
            case Prefix:     return convertPrefix     (tree, level);
            case Postfix:    return convertPostfix    (tree, level);
            default: throw new AssertionError("Invalid shit; GTFO");
                // clang-format on
        }
    }

    protected String conv(ArrayList<NodeTree> nodes, int level, String sep) {
        return String.join(
            sep,
            nodes.stream()
                .map(sn -> conv(sn, level))
                .collect(Collectors.toList()));
    }
}
