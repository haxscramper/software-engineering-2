package code_translator_PinC;

import java.util.ArrayList;
import utilities.PointedException;

// Сканнер. Разбивает исходную программу на лексемы
public class Scanner
{

    private TextDriver source; // Ссылка на поток исходного кода

    private char symbol; // Обрабатываемый символ

    private int line_position;   // Текущий номер линии
    private int symbol_position; // Текущий номер символа

    // Иницализация сканера
    public Scanner(TextDriver source) {

        this.source = source;
        symbol      = ' ';
    }

    // Создание таблицы лексем по тексту
    public ArrayList<Lexeme> analysis() throws PointedException {

        source.dropping();

        ArrayList<Lexeme> lexemes = new ArrayList<Lexeme>();
        Lexeme            lexeme;

        do {

            lexeme = read_lexeme();
            lexemes.add(lexeme);
        } while (lexeme.get_type() != types_lexeme.lex_End);

        return lexemes;
    }

    // Взятие следующей лексемы из потока исходного текста
    Lexeme read_lexeme() throws PointedException {

        while (symbol == '\n' || symbol == '\t' || symbol == ' '
               || symbol == 13)
            next_symbol();

        line_position   = source.get_line_position();
        symbol_position = source.get_symbol_position();

        if (is_symbol()) {

            return identifier_read();
        } else if (is_digit()) {

            return number_read();
        } else {

            switch (symbol) {

                case ':':
                    next_symbol();
                    if (symbol == '=') {

                        next_symbol();
                        return new Lexeme(
                            types_lexeme.lex_assignment,
                            ":=",
                            line_position,
                            symbol_position);
                    } else {

                        return new Lexeme(
                            types_lexeme.lex_colon,
                            ":",
                            line_position,
                            symbol_position);
                    }
                case '>':
                    next_symbol();
                    if (symbol == '=') {

                        next_symbol();
                        return new Lexeme(
                            types_lexeme.lex_more_equal,
                            ">=",
                            line_position,
                            symbol_position);
                    } else {

                        return new Lexeme(
                            types_lexeme.lex_more,
                            ">",
                            line_position,
                            symbol_position);
                    }
                case '<':
                    next_symbol();
                    if (symbol == '=') {

                        next_symbol();
                        return new Lexeme(
                            types_lexeme.lex_less_equal,
                            "<=",
                            line_position,
                            symbol_position);
                    } else if (symbol == '>') {

                        next_symbol();
                        return new Lexeme(
                            types_lexeme.lex_not_equal,
                            "<>",
                            line_position,
                            symbol_position);
                    } else {

                        return new Lexeme(
                            types_lexeme.lex_less,
                            "<",
                            line_position,
                            symbol_position);
                    }
                case '.':
                    next_symbol();
                    if (symbol == '.') {

                        next_symbol();
                        return new Lexeme(
                            types_lexeme.lex_double_point,
                            "..",
                            line_position,
                            symbol_position);
                    } else {

                        return new Lexeme(
                            types_lexeme.lex_point,
                            ".",
                            line_position,
                            symbol_position);
                    }
                case '/':

                    next_symbol();
                    if (symbol == '/') {

                        while (symbol != '\n')
                            next_symbol();

                        return read_lexeme();
                    } else {

                        return new Lexeme(
                            types_lexeme.lex_division,
                            "/",
                            line_position,
                            symbol_position);
                    }
                case '{':

                    while (symbol != '}') {

                        next_symbol();

                        if (symbol == '\0')
                            return read_lexeme();
                    }

                    next_symbol();
                    return read_lexeme();
                case '\'':

                    String value = "";
                    next_symbol();

                    while (symbol != '\'') {

                        value += symbol;
                        next_symbol();
                        if (symbol == '\0')
                            throw error("Ожидался конец строчного литерала");
                    }

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_String,
                        value,
                        line_position,
                        symbol_position);
                case '+':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_plus,
                        "+",
                        line_position,
                        symbol_position);
                case '-':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_minus,
                        "-",
                        line_position,
                        symbol_position);
                case '*':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_mult,
                        "*",
                        line_position,
                        symbol_position);
                case '=':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_equal,
                        "=",
                        line_position,
                        symbol_position);
                case '[':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_sq_bracket_L,
                        "]",
                        line_position,
                        symbol_position);
                case ']':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_sq_bracket_R,
                        "[",
                        line_position,
                        symbol_position);
                case '(':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_bracket_L,
                        "(",
                        line_position,
                        symbol_position);
                case ')':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_bracket_R,
                        ")",
                        line_position,
                        symbol_position);
                case ',':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_comma,
                        ",",
                        line_position,
                        symbol_position);
                case ';':

                    next_symbol();
                    return new Lexeme(
                        types_lexeme.lex_semicolon,
                        ";",
                        line_position,
                        symbol_position);
                case '\0':

                    return new Lexeme(
                        types_lexeme.lex_End,
                        "end file",
                        line_position,
                        symbol_position);

                default:

                    throw new PointedException(
                        "Данный символ не принадлежит алфавиту языка паскаль",
                        line_position,
                        symbol_position,
                        line_position,
                        symbol_position);
            }
        }
    }

    // Функции чтения для идентификаторов и чисел
    private Lexeme identifier_read() {

        String value = "";

        do {

            value += symbol;
            next_symbol();
        } while (is_symbol());

        Lexeme test = keyword_check(value);

        if (test.get_type() == types_lexeme.lex_Name) {

            while (is_symbol() || is_digit()) {

                value += symbol;
                next_symbol();
            }

            return new Lexeme(
                types_lexeme.lex_Name, value, line_position, symbol_position);
        } else {

            return test;
        }
    }
    private Lexeme number_read() {

        String value = "";

        do {

            value += symbol;
            next_symbol();
        } while (is_digit());

        if (symbol == '.') {

            do {

                value += symbol;
                next_symbol();
            } while (is_digit());

            return new Lexeme(
                types_lexeme.lex_Real, value, line_position, symbol_position);
        } else {

            return new Lexeme(
                types_lexeme.lex_Number, value, line_position, symbol_position);
        }
    }

    // Проверка идентификатора на принадлежность к ключевому слову
    private Lexeme keyword_check(String value) {

        String value_lowerCase = value.toLowerCase();

        switch (value_lowerCase) {

            case "and":
                return new Lexeme(
                    types_lexeme.lex_and,
                    value,
                    line_position,
                    symbol_position);
            case "array":
                return new Lexeme(
                    types_lexeme.lex_array,
                    value,
                    line_position,
                    symbol_position);
            case "begin":
                return new Lexeme(
                    types_lexeme.lex_begin,
                    value,
                    line_position,
                    symbol_position);
            case "case":
                return new Lexeme(
                    types_lexeme.lex_case,
                    value,
                    line_position,
                    symbol_position);
            case "const":
                return new Lexeme(
                    types_lexeme.lex_const,
                    value,
                    line_position,
                    symbol_position);
            case "div":
                return new Lexeme(
                    types_lexeme.lex_div,
                    value,
                    line_position,
                    symbol_position);
            case "do":
                return new Lexeme(
                    types_lexeme.lex_do, value, line_position, symbol_position);
            case "downto":
                return new Lexeme(
                    types_lexeme.lex_downto,
                    value,
                    line_position,
                    symbol_position);
            case "else":
                return new Lexeme(
                    types_lexeme.lex_else,
                    value,
                    line_position,
                    symbol_position);
            case "end":
                return new Lexeme(
                    types_lexeme.lex_end,
                    value,
                    line_position,
                    symbol_position);
            case "file":
                return new Lexeme(
                    types_lexeme.lex_file,
                    value,
                    line_position,
                    symbol_position);
            case "for":
                return new Lexeme(
                    types_lexeme.lex_for,
                    value,
                    line_position,
                    symbol_position);
            case "function":
                return new Lexeme(
                    types_lexeme.lex_function,
                    value,
                    line_position,
                    symbol_position);
            case "goto":
                return new Lexeme(
                    types_lexeme.lex_goto,
                    value,
                    line_position,
                    symbol_position);
            case "if":
                return new Lexeme(
                    types_lexeme.lex_if, value, line_position, symbol_position);
            case "in":
                return new Lexeme(
                    types_lexeme.lex_in, value, line_position, symbol_position);
            case "label":
                return new Lexeme(
                    types_lexeme.lex_label,
                    value,
                    line_position,
                    symbol_position);
            case "mod":
                return new Lexeme(
                    types_lexeme.lex_mod,
                    value,
                    line_position,
                    symbol_position);
            case "nil":
                return new Lexeme(
                    types_lexeme.lex_nil,
                    value,
                    line_position,
                    symbol_position);
            case "not":
                return new Lexeme(
                    types_lexeme.lex_not,
                    value,
                    line_position,
                    symbol_position);
            case "of":
                return new Lexeme(
                    types_lexeme.lex_of, value, line_position, symbol_position);
            case "or":
                return new Lexeme(
                    types_lexeme.lex_or, value, line_position, symbol_position);
            case "packed":
                return new Lexeme(
                    types_lexeme.lex_packed,
                    value,
                    line_position,
                    symbol_position);
            case "procedure":
                return new Lexeme(
                    types_lexeme.lex_procedure,
                    value,
                    line_position,
                    symbol_position);
            case "program":
                return new Lexeme(
                    types_lexeme.lex_program,
                    value,
                    line_position,
                    symbol_position);
            case "record":
                return new Lexeme(
                    types_lexeme.lex_record,
                    value,
                    line_position,
                    symbol_position);
            case "repeat":
                return new Lexeme(
                    types_lexeme.lex_repeat,
                    value,
                    line_position,
                    symbol_position);
            case "set":
                return new Lexeme(
                    types_lexeme.lex_set,
                    value,
                    line_position,
                    symbol_position);
            case "then":
                return new Lexeme(
                    types_lexeme.lex_then,
                    value,
                    line_position,
                    symbol_position);
            case "to":
                return new Lexeme(
                    types_lexeme.lex_to, value, line_position, symbol_position);
            case "type":
                return new Lexeme(
                    types_lexeme.lex_type,
                    value,
                    line_position,
                    symbol_position);
            case "until":
                return new Lexeme(
                    types_lexeme.lex_until,
                    value,
                    line_position,
                    symbol_position);
            case "var":
                return new Lexeme(
                    types_lexeme.lex_var,
                    value,
                    line_position,
                    symbol_position);
            case "while":
                return new Lexeme(
                    types_lexeme.lex_while,
                    value,
                    line_position,
                    symbol_position);
            case "with":
                return new Lexeme(
                    types_lexeme.lex_with,
                    value,
                    line_position,
                    symbol_position);

            case "integer":
                return new Lexeme(
                    types_lexeme.lex_integer,
                    value,
                    line_position,
                    symbol_position);
            case "smallint":
                return new Lexeme(
                    types_lexeme.lex_smallint,
                    value,
                    line_position,
                    symbol_position);
            case "longint":
                return new Lexeme(
                    types_lexeme.lex_longint,
                    value,
                    line_position,
                    symbol_position);
            case "real":
                return new Lexeme(
                    types_lexeme.lex_real,
                    value,
                    line_position,
                    symbol_position);
            case "boolean":
                return new Lexeme(
                    types_lexeme.lex_boolean,
                    value,
                    line_position,
                    symbol_position);
            case "string":
                return new Lexeme(
                    types_lexeme.lex_string,
                    value,
                    line_position,
                    symbol_position);
            case "char":
                return new Lexeme(
                    types_lexeme.lex_char,
                    value,
                    line_position,
                    symbol_position);
            case "byte":
                return new Lexeme(
                    types_lexeme.lex_byte,
                    value,
                    line_position,
                    symbol_position);

            default:
                return new Lexeme(
                    types_lexeme.lex_Name,
                    value,
                    line_position,
                    symbol_position);
        }
    }

    // Функции проверки на принаблежность символа определенной группе
    private boolean is_symbol() {

        return (symbol >= 'A' && symbol <= 'Z')
            || (symbol >= 'a' && symbol <= 'z');
    }
    private boolean is_digit() { return (symbol >= '0' && symbol <= '9'); }

    // Берем следущий символ
    private void next_symbol() { symbol = source.take_next_symbol(); }

    // Генератор ошибок
    private PointedException error(String message) {

        return new PointedException(
            message,
            line_position,
            symbol_position,
            line_position,
            symbol_position);
    }
}
