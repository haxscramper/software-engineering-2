package code_translator_PinC;

import java.util.EnumSet;

public enum nk {

    Maincode,
    Block, // обозначения блока (совокупности инструкций)n
    ProcDecl,  // для функций и процедур
    FuncDecl,  // для функций и процедур
    ParamList, /// Function/procedure parameter list
    VarSection,
    Empty,
    Par, /// Parentehsis

    // литералы
    Int,
    Real,
    Str,

    // инструкции
    For,
    While,
    Do,
    Repeat,
    If,
    ElseIf,
    Else,
    Asgn,    // присваивание
    Call,    // вызов процедуры/функции
    VarDecl, // объявление
    Expr,
    RetStmt,

    //выражения
    Ident,   // идентификатор
    OpIdent, // оператор
    Infix,
    Prefix,
    Postfix;
    public boolean in(EnumSet<nk>... sets) {
        for (EnumSet<nk> set : sets) {
            if (set.contains(this)) {
                return true;
            }
        }

        return false;
    }

    public void expectKind(EnumSet<nk>... kinds) {
        EnumSet<nk> expected = EnumSet.noneOf(nk.class);

        boolean found = false;
        for (EnumSet<nk> set : kinds) {
            expected.addAll(set);
            if (this.in(set)) {
                found = true;
            }
        }

        assert found
            : "Expected any of "
              + expected.toString()
              + ", but got kind"
              + this.toString();
    }

    public void expectKind(nk... kinds) {
        EnumSet<nk> tmp = EnumSet.noneOf(nk.class);
        for (nk k : kinds) {
            tmp.add(k);
        }

        expectKind(tmp);
    }


    public static final EnumSet<nk> ExprKinds;
    public static final EnumSet<nk> StmtKinds;
    public static final EnumSet<nk> LiterKinds;
    public static final EnumSet<nk> StrKinds;

    static {
        StrKinds   = EnumSet.of(nk.Str, nk.Ident, nk.OpIdent);
        LiterKinds = EnumSet.of(nk.Str, nk.Real, nk.Int, nk.Ident, nk.OpIdent);
        ExprKinds  = EnumSet.of(
            nk.Infix, nk.Prefix, nk.Postfix, nk.Call, nk.Expr);
        // TokKinds = EnumSet.of(nk.OpIdent);
        StmtKinds = EnumSet.of(
            nk.VarDecl,
            nk.If,
            nk.ElseIf,
            nk.Else,
            nk.Asgn,
            nk.ProcDecl,
            nk.Block,
            nk.Maincode,
            nk.ParamList,
            nk.FuncDecl,
            nk.Par,
            nk.While);
    }

    public boolean hasSubnodes() { return this.in(ExprKinds, StmtKinds); }
}
