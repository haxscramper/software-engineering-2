package code_translator_PinC;

import static code_translator_PinC.StringUtils.*;

import code_translator_PinC.*;
import java.util.ArrayList;
import java.util.stream.*;

public abstract class CLikeConverter extends ConverterBase
{

    public String convertIf(NodeTree tree, int level) {
        String pref = repeat(" ", level * 2);
        String res  = "";
        int    idx  = 0;

        for (NodeTree sn : tree.getSubnodes()) {
            if (idx == 0) {
                res += pref + fmt("if (%s) {\n", conv(sn.get(0), level + 1));
            } else {
                if (sn.getKind() == nk.ElseIf) {
                    res += pref
                           + fmt(
                               "else if (%s) {\n", conv(sn.get(0), level + 1));
                } else {
                    res += pref + fmt("else {\n");
                }
            }

            res += conv(sn.get((sn.getKind() == nk.Else) ? 0 : 1), level + 1)
                   + "\n";
            // if  {
            // } else {
            //     res += conv(sn.get(1), level + 1) + "\n";
            // }


            if (idx == tree.size() - 1) {
                res += pref + "}\n";
            }
            ++idx;

            // return fmt("%sif (%s) {\n%s%s\n}", pref,
            // conv(tree.get(0), level + 1),
            // conv(tree.get(0), level + 1));
        }
        return res;
    }

    public String convertAsgn(NodeTree tree, int level) {
        String pref = repeat(" ", level * 2);
        return fmt(
            "%s%s = %s;",
            pref,
            conv(tree.get(0), level + 1),
            conv(tree.get(1), level + 1));
    }


    public String convertBlock(NodeTree tree, int level) {
        String pref = repeat(" ", level * 2);
        return conv(tree.getSubnodes(), level + 1, "\n");
    }

    public String convertFuncDecl(NodeTree tree, int level) {
        return convertProcDecl(tree, level);
    }

    public String convertParamList(NodeTree tree, int level) {
        return conv(tree.getSubnodes(), level, ", ");
    }

    public String convertVarDecl(NodeTree tree, int level) {
        NodeTree varType = tree.get(tree.size() - 1);
        String   res     = "";

        for (int idx = 0; idx < tree.size() - 1; ++idx) {
            if (idx != 0) {
                res += ", ";
            }
            res += fmt(
                "%s %s",
                convertTypeNode(varType),
                conv(tree.get(idx), level + 1));
        }

        return res;
    }

    public String convertVarSection(NodeTree tree, int level) {
        return "<<VAR-SECTION>>";
    }


    public String convertFor(NodeTree tree, int level) { return "<<FOR>>"; }

    public String convertWhile(NodeTree tree, int level) {
        String pref = repeat(" ", level * 2);
        return pref + fmt("while (%s) {\n", conv(tree.get(0), level + 1))
            + conv(tree.get(1), level + 1) + fmt("\n%s}", pref);
    }

    public String convertDo(NodeTree tree, int level) { return "<<DO>>"; }
    public String convertRepeat(NodeTree tree, int level) {
        return "<<<REPEAT>>>";
        // return fmt("%swhile (%s) {\n%s$\n}")
    }

    public String convertElseIf(NodeTree tree, int level) {
        return "<<ELSEIF>>";
    }
    public String convertElse(NodeTree tree, int level) { return "<<ELSE>>"; }
    public String convertCall(NodeTree tree, int level) { return "<<CALL>>"; }
    public String convertRetStmt(NodeTree tree, int level) {
        return "<<RET-STMT>>";
    }
}
