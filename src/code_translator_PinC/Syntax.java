package code_translator_PinC;

import static code_translator_PinC.NodeTree.tree;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.*;
import utilities.PointedException;

public class Syntax
{
    private final static Logger l = Logger.getLogger("parseLogger");

    public static void toggleLogging(boolean doLogging) {
        if (doLogging) {
            ConsoleHandler handler = new ConsoleHandler();
            l.setUseParentHandlers(false);

            handler.setFormatter(new SimpleFormatter() {
                @Override
                public synchronized String format(LogRecord lr) {
                    return String.format(
                        "[\033[32m%1$-7s\033[39m] %2$s %n",
                        lr.getLevel().getLocalizedName(),
                        lr.getMessage());
                }
            });

            System.out.printf("Add logging handler\n");
            if (l.getHandlers().length < 1) {
                l.addHandler(handler);
            }
        } else {
            for (Handler h : l.getHandlers()) {
                System.out.printf("Toggle logging off\n");
                l.removeHandler(h);
            }
        }
    }


    static { toggleLogging(true); }

    // Информация для компилятора
    private String name_func;

    // Данные для анализа
    private ArrayList<Lexeme> lexemes;
    private TextDriver        source;
    private int               index;

    public NodeTree analysis() throws PointedException {
        return read_program();
    }

    // Функции анализирования синтаксических конструкций
    private NodeTree read_program() throws PointedException {
        NodeTree res = new NodeTree(nk.Maincode);
        l.info("Reading main program");

        if (lexType() == types_lexeme.lex_program) {
            next_lexeme();

            if (lexType() == types_lexeme.lex_Name) {
                next_lexeme();
                skipCurrSemicolon();
            } else {
                throw error("Ожидалось имя");
            }
        } else {
            l.info("No explicit program declaration");
        }

        if (lexType() == types_lexeme.lex_var) { // Глобальные переменные
            res.add(read_var_section());
        } else {
            l.info("No global variables found");
        }

        do {
            if (lexType() == types_lexeme.lex_procedure) {
                res.add(read_procedure());
            } else if (lexType() == types_lexeme.lex_function) {
                res.add(read_function());
            } else {
                l.info("No more procedures declared");
                break;
            }


        } while (true);

        if (lexType() == types_lexeme.lex_begin) { // Главная функция
            res.add(read_block());
            if (lexType() != types_lexeme.lex_point) {
                throw error("Ожидалась точка");
            }

        } else {
            // print_around(5);
            throw error("Ожидалось начало блока");
        }

        return res;
    }


    private NodeTree read_variable() throws PointedException {
        NodeTree res = new NodeTree(nk.VarDecl);
        while (true) {
            if (lexType() == types_lexeme.lex_Name) {
                res.add(nk.Ident, lexValue());
                next_lexeme();
                if (lexType() == types_lexeme.lex_comma) {
                    next_lexeme();

                } else if (lexType() == types_lexeme.lex_colon) {
                    next_lexeme();
                    break;

                } else {
                    throw error("Ожидалось \',\' или \':\'");
                }
            }
        }

        if (is_type_variable()) {
            res.add(nk.Ident, lexValue());
            next_lexeme();
        } else {

            throw error("Ожидался тип переменной");
        }

        return res;
    }

    private NodeTree read_var_section() throws PointedException {
        NodeTree res = new NodeTree(nk.VarSection);

        assert lexType()
            == types_lexeme.lex_var
            : "Var section parse must start with `lex_var`";

        next_lexeme();

        do {
            res.add(read_variable());
            skipCurrSemicolon();
        } while (lexType() == types_lexeme.lex_Name);
        return res;
    }

    private NodeTree read_parameter() throws PointedException {
        NodeTree res = new NodeTree(nk.VarDecl);
        while (true) {
            if (lexType() == types_lexeme.lex_Name) {
                res.add(nk.Ident, lexValue());
                next_lexeme();
                if (lexType() == types_lexeme.lex_comma) {
                    next_lexeme();
                } else if (lexType() == types_lexeme.lex_colon) {
                    next_lexeme();
                    break;
                } else {
                    throw error("Ожидалось \',\' или \':\'");
                }
            }
        }

        if (is_type_variable()) {
            res.add(nk.Ident, lexStr());
            next_lexeme();
        } else {
            throw error("Ожидался тип переменной");
        }

        return res;
    }


    /// Parse single `if` statement starting from current position.
    private NodeTree[] read_if() throws PointedException {
        assert lexType()
            == types_lexeme.lex_if : "If parse must start with `if` token";

        next_lexeme();
        NodeTree ifCond  = read_expression();
        NodeTree ifBlock = tree(nk.Block);
        if (lexType() == types_lexeme.lex_then) {
            next_lexeme();
            ifBlock = read_block();
            l.info(ifBlock.lispRepr());
        } else {
            throw error("Expected `then`");
        }

        l.info("Finished parsing if statement");

        NodeTree[] res = {ifCond, ifBlock};
        return res;
    }

    private NodeTree read_block() throws PointedException {
        boolean  multiBlock = true;
        NodeTree res        = new NodeTree(nk.Block);

        l.info("Reading block");
        // print_around(5);

        if (lexType() == types_lexeme.lex_begin) {
            next_lexeme();
        } else if (lexType() == types_lexeme.lex_repeat) {
            next_lexeme();
            res = new NodeTree(nk.Repeat);
        } else {
            multiBlock = false;
        }

        do {
            if (lexType() == types_lexeme.lex_Name) {
                print_around(2);
                String name = lexValue();
                next_lexeme();
                if (lexType() == types_lexeme.lex_assignment) {
                    next_lexeme();
                    res.add(nk.Asgn, tree(nk.Ident, name), read_expression());
                    skipCurrSemicolon();
                } else if (lexType() == types_lexeme.lex_assignment) {
                    next_lexeme();
                    res.add(nk.RetStmt, read_expression());
                    skipCurrSemicolon();
                } else if (lexType() == types_lexeme.lex_bracket_L) {
                    next_lexeme();
                    if (lexType() == types_lexeme.lex_bracket_R) {
                        next_lexeme();
                    } else {
                        while (true) {
                            NodeTree expr = read_expression();
                            if (lexType() == types_lexeme.lex_comma) {
                                next_lexeme();
                                continue;
                            } else if (
                                lexType() == types_lexeme.lex_bracket_R) {
                                next_lexeme();
                                break;
                            } else {
                                throw error("Ошибка чтения функции");
                            }
                        }
                    }

                    skipCurrSemicolon();
                } else {
                    throw error("Expected assignment or left bracket");
                }
            } else if (lexType() == types_lexeme.lex_while) {
                // l.info(res.lispRepr());
                NodeTree ex = read_expression();
                l.info("Finished expresssion");
                l.info(ex.lispRepr());

                NodeTree wh = new NodeTree(nk.While);
                wh.add(ex);
                // next_lexeme();
                if (lexType() == types_lexeme.lex_do) {
                    next_lexeme();
                    if (lexType() == types_lexeme.lex_begin) {
                        wh.add(read_block());
                        skipCurrSemicolon();
                    } else {
                        wh.add(read_block());
                    }
                } else {
                    throw error("Ожидалось do");
                }

                res.add(wh);


            } else if (lexType() == types_lexeme.lex_repeat) {
                res.add(nk.Do, read_block(), read_expression());
                skipCurrSemicolon();
            } else if (lexType() == types_lexeme.lex_if) { // Операция if
                NodeTree ifRes = tree(nk.If);

                // Read all `if` statements
                while (lexType() == types_lexeme.lex_if) {
                    NodeTree[] branch = read_if();
                    ifRes.add(tree(nk.ElseIf, branch[0], branch[1]));
                }

                if (lexType() == types_lexeme.lex_else) {
                    next_lexeme();
                    if (lexType() == types_lexeme.lex_begin) {
                        ifRes.add(tree(nk.Else, read_block()));
                        skipCurrSemicolon();
                    } else {
                        ifRes.add(tree(nk.Else, read_block()));
                    }
                }

                res.add(ifRes);

            } else if (
                lexType() == types_lexeme.lex_end
                && res.getKind() == nk.Block) {
                next_lexeme();
                return res;
            } else if (
                lexType() == types_lexeme.lex_until
                && res.getKind() == nk.Repeat) {

                next_lexeme();
                return res;
            } else {

                throw error("Ошибка блока");
            }

        } while (multiBlock);
        return res;
    }

    /// FIXME parse expression
    private NodeTree read_expression() throws PointedException {
        NodeTree res = new NodeTree(nk.Expr);

        // print_around(2);
        l.info("Reading expression");
        while (true) {
            switch (lexType()) {
                case lex_then:
                case lex_end:
                case lex_do:
                case lex_semicolon: {
                    // print_around(10);
                    // l.info(res.lispRepr());
                    return res;
                }
                default: {
                    res.add(lex());
                    next_lexeme();
                }
            }
        }
    }
    private NodeTree read_procedure() throws PointedException {

        String            parameters = "";
        ArrayList<String> variables  = new ArrayList<String>();


        assert   lexType() == types_lexeme.lex_procedure;
        NodeTree res = new NodeTree(lex());
        next_lexeme();

        if (lexType() == types_lexeme.lex_Name) {
            res.add(lex());
            next_lexeme();
            if (lexType() == types_lexeme.lex_bracket_L) {
                next_lexeme();
                if (lexType() == types_lexeme.lex_bracket_R) {
                    next_lexeme();
                    res.add(nk.Empty);
                } else {
                    res.add(read_param_list());
                }
            } else {
                throw error("Ожидался символ \'(\'");
            }
        } else {
            throw error("Ожидалось имя");
        }

        if (lexType() == types_lexeme.lex_var) {
            res.add(read_var_section());
        } else {
            res.add(nk.Empty);
        }

        if (lexType() == types_lexeme.lex_begin) {
            res.add(read_block());
            skipCurrSemicolon();
        } else {
            throw error("Ожидалось тело процедуры");
        }

        return res;
    }

    private NodeTree read_param_list() throws PointedException {
        NodeTree params = new NodeTree(nk.ParamList);
        if (lexType() == types_lexeme.lex_bracket_R) {
            next_lexeme();
        } else {
            while (true) {
                params.add(read_parameter());
                if (lexType() == types_lexeme.lex_semicolon) {
                    next_lexeme();
                    continue;
                } else if (lexType() == types_lexeme.lex_bracket_R) {
                    next_lexeme();
                    break;
                } else {
                    throw error("Ожидалось точка с запятой");
                }
            }
        }

        return params;
    }

    private NodeTree read_function() throws PointedException {
        String type_func = "";
        next_lexeme();
        NodeTree res = new NodeTree(nk.FuncDecl);


        if (lexType() == types_lexeme.lex_Name) {
            res.add(nk.Ident, lexValue()); // Function name
            l.info(String.format("Found function [%1$s]", res.get(0).getStr()));
            next_lexeme();
            if (lexType() == types_lexeme.lex_bracket_L) {
                next_lexeme();
                res.add(read_param_list());
                if (lexType() == types_lexeme.lex_colon) {
                    next_lexeme();
                    if (is_type_variable()) {
                        res.add(nk.Ident, lexStr());
                        next_lexeme();
                    }
                } else {
                    throw error("Ожидалась объявление типа");
                }

                skipCurrSemicolon();
            } else {
                throw error("Ожидался символ \'(\'");
            }
        } else {
            throw error("Ожидалось имя");
        }

        if (lexType() == types_lexeme.lex_var) {
            next_lexeme();
            do {
                res.add(read_variable());
                skipCurrSemicolon();
            } while (lexType() == types_lexeme.lex_Name);
        }

        l.info(res.treeRepr(0));


        if (lexType() == types_lexeme.lex_begin) {
            res.add(read_block());
            skipCurrSemicolon();
        } else {
            throw error("Ожидалось тело функции");
        }

        return res;
    }

    // Функции принадлежности лексем к определенной группе
    private boolean is_type_variable() {

        return lexType() == types_lexeme.lex_integer
            || lexType() == types_lexeme.lex_real
            || lexType() == types_lexeme.lex_char
            || lexType() == types_lexeme.lex_boolean;
    }
    private boolean is_member_expression() {

        return lexType() == types_lexeme.lex_Number
            || lexType() == types_lexeme.lex_Real;
    }
    private boolean is_operator() {

        return lexType().ordinal() >= types_lexeme.lex_plus.ordinal()
            && lexType().ordinal() <= types_lexeme.lex_or.ordinal();
    }


    // Конструктор синтаксического анализатора
    public Syntax(ArrayList<Lexeme> lexemes, TextDriver source) {
        this.lexemes = lexemes;
        this.source  = source;
        index        = 0;
        name_func    = " ";
    }

    private void next_lexeme() { index++; }

    private types_lexeme lexType() { return lexemes.get(index).get_type(); }

    private String lexStr() { return lex().toStr(); }
    private String lexValue() { return lexemes.get(index).get_value(); }
    private Lexeme lex() { return lexemes.get(index); }

    private void prev_lexeme() {
        index--;
        // index--;
        // next_lexeme();
    }

    private void print_around(int behind) {
        for (int i = index - behind; i < index + 2; ++i) {
            if (i < 0) {
                continue;
            } else if (i >= lexemes.size()) {
                break;
            }

            Lexeme lex = lexemes.get(i);
            String fmt = "(%3d) [ %-10s]";
            if (i == index) {
                fmt = "\033[32m" + fmt + "\033[39m";
            }

            fmt += " %-20s";

            l.info(String.format(
                fmt, i, lex.get_value(), lex.get_type().toString()));
        }
    }

    private void skipCurrSemicolon() throws PointedException {
        if (lexType() == types_lexeme.lex_semicolon) {
            next_lexeme();
        } else {
            throw error(String.format(
                "Ожидалась точка с запятой, но был встречен '%1s' (%2s)",
                lexValue(),
                lexStr()));
        }
    }

    // Генерация ошибки
    private PointedException error(String message) {
        return new PointedException(
            message,
            lex().get_line_position(),
            lex().get_symbol_position(),
            lex().get_line_position(),
            lex().get_symbol_position() + lex().toStr().length());
    }
    private PointedException error_k(String message) {
        return new PointedException(
            message,
            lex().get_line_position(),
            lex().get_symbol_position(),
            lex().get_line_position(),
            lex().get_symbol_position() + lex().toStr().length());
    }
}
