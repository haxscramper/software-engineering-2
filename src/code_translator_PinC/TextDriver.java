package code_translator_PinC;

import java.io.*;
import java.util.ArrayList;

//Текстовый драйвер
public class TextDriver
{

    private ArrayList<String> source; // Контейнер с исходным кодом
    private int line_position; // Номер текущей линии для считывания
                               // символа
    private int symbol_position; // Номер текущего символа для считывания

    // Конструктор драйвера. Загружает текст из файла
    /*TextDriver (String address) {

        String line;
        source = new ArrayList <String>();
        line_position = 0;
        symbol_position = 0;

        try {

            File file = new File(address);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

           line = reader.readLine();

           while (line != null) {

               line += '\n';
               source.add(line);
               line = reader.readLine();
           }

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        source.add(source.size() - 1, source.get(source.size() - 1) +
    "\0");
    }*/

    public TextDriver(String code) {

        String line;
        source          = new ArrayList<String>();
        line_position   = 0;
        symbol_position = 0;
        int i           = 0;

        while (i < code.length()) {

            line = "";

            while (i < code.length() && code.charAt(i) != '\n') {

                if (code.charAt(i) != '\t') {
                    line += code.charAt(i);
                }

                i++;
            }

            line += '\n';
            i++;
            source.add(line);
        }

        source.add(
            source.size() - 1, source.get(source.size() - 1) + "\0");
    }

    // Сброс позиции по тексту
    void dropping() {

        line_position   = 0;
        symbol_position = 0;
    }

    // Функция берет следущий символ
    char take_next_symbol() {

        while (source.get(line_position).length() == symbol_position) {

            symbol_position = 0;
            line_position++;
        }

        return source.get(line_position).charAt(symbol_position++);
    }

    // Функции возвращают текущее положение на текстовом файле
    int get_line_position() { return line_position + 1; }
    int get_symbol_position() { return symbol_position; }

    // Функции возвращающию определенную линию из текста для вывода ошибок
    String get_line() { return source.get(line_position); }
    String get_line(int index) { return source.get(index - 1); }

    // Функция вывода для отладки
    void print_text() {

        int length = source.size();

        for (int i = 0; i < length; i++)
            System.out.print("" + i + "\t| " + source.get(i));
    }
}
