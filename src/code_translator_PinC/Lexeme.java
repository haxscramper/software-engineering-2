package code_translator_PinC;

import java.util.ArrayList;

// Класс содержит описание всех лексических конструкций языка Pascal
public class Lexeme
{

    final private types_lexeme type;  // Тип лексемы
    final private String       value; // Ее значение

    final private int line_position; // Ее позиция по линии (для отладки)
    final private int symbol_position; // Ее позиция по символам (для
                                       // отладки)

    // Конструкторы лексем
    Lexeme() {

        type            = types_lexeme.lex_None;
        value           = "";
        line_position   = 0;
        symbol_position = 0;
    }
    Lexeme(
        types_lexeme type,
        String       value,
        int          line_position,
        int          symbol_position) {

        this.type            = type;
        this.value           = value;
        this.line_position   = line_position;
        this.symbol_position = symbol_position;
    }

    // Функции взятия информации о лексеме
    types_lexeme  get_type() { return type; }
    public String get_value() { return value; }
    int           get_line_position() { return line_position; }
    int           get_symbol_position() { return symbol_position; }


    public String toStr() {
        switch (type) {
            case lex_Name:
            case lex_Number: {
                return value;
            }

            default: return type.toStr();
        }
    }

    void print() {

        System.out.printf(
            "%-20s%-20s%-7s%-7s\n",
            type,
            value,
            line_position,
            symbol_position);
    }

    static void print_table(ArrayList<Lexeme> lexemes) {

        System.out.printf(
            "%-20s%-20s%-7s%-7s\n",
            "тип лексемы",
            "значение лексемы",
            "линия",
            "символ");
        for (int i = 0; i < lexemes.size(); i++) {

            lexemes.get(i).print();
        }
    }
}

//Перечисление типов лексем
enum types_lexeme
{

    lex_None, // Пустая лексема
    lex_Name, // Лексема имени

    // Лекскмы литералов
    lex_Number,
    lex_Real,
    lex_String,

    // Лексемы ключевых слов языка Pascal
    lex_array,
    lex_begin,
    lex_case,
    lex_const,
    lex_do,
    lex_downto,
    lex_else,
    lex_end,
    lex_file,
    lex_for,
    lex_function,
    lex_goto,
    lex_if,
    lex_in,
    lex_label,
    lex_nil,
    lex_of,
    lex_packed,
    lex_procedure,
    lex_program,
    lex_record,
    lex_repeat,
    lex_set,
    lex_then,
    lex_to,
    lex_type,
    lex_until,
    lex_var,
    lex_while,
    lex_with,

    // Лексемы типов
    lex_integer,
    lex_smallint,
    lex_longint,
    lex_real,
    lex_boolean,
    lex_string,
    lex_char,
    lex_byte,

    // Лексемы оставшихся опреаторов (часть операторов являются ключевыми
    // словами)
    lex_assignment, // :=
    lex_plus,       // +
    lex_minus,      // -
    lex_mult,       // *
    lex_division,   // /
    lex_div,        // div
    lex_mod,        // mod

    lex_equal,      // =
    lex_more,       // >
    lex_less,       // <
    lex_more_equal, // >=
    lex_less_equal, // <=
    lex_not_equal,  // <>
    lex_and,        // and
    lex_or,         // or
    lex_not,        // not

    lex_sq_bracket_L, // [
    lex_sq_bracket_R, // ]
    lex_bracket_L,    // (
    lex_bracket_R,    // )
    lex_point,        // .
    lex_comma,        // ,
    lex_colon,        // :
    lex_semicolon,    // ;
    lex_double_point, // ..

    lex_End; // Лексема конца программы

    public String toStr() {
        switch (this) {
            case lex_integer: return "int";
            case lex_smallint: return "short int";
            case lex_longint: return "long int";
            case lex_real: return "double";
            case lex_boolean: return "bool";
            case lex_string: return "string";
            case lex_char: return "char";
            case lex_byte: return "char";

            case lex_assignment: return "=";
            case lex_plus: return "+";
            case lex_minus: return "-";
            case lex_mult: return "*";
            case lex_division: return "/";
            case lex_div: return "/";
            case lex_mod: return "%";

            case lex_equal: return "==";
            case lex_more: return ">";
            case lex_less: return "<";
            case lex_more_equal: return ">=";
            case lex_less_equal: return "<=";
            case lex_not_equal: return "!=";
            case lex_not: return "!";
            case lex_and: return "&&";
            case lex_or: return "||";

            case lex_sq_bracket_L: return "[";
            case lex_sq_bracket_R: return "]";
            case lex_bracket_L: return "(";
            case lex_bracket_R: return ")";
            case lex_comma: return ",";

            case lex_begin: return "begin";
            case lex_then: return "then";

            default: return this.toString();
        }
    }
}
