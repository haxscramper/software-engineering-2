package code_translator_PinC;

public class StringUtils
{
    public static String joinl(String... args) {
        String res = "";
        int    idx = 0;
        for (String s : args) {
            if (idx != 0) {
                res += "\n";
            }
            res += s;
            ++idx;
        }

        return res;
    }

    public static String indent(String str, int level) {
        String res = "";
        int    idx = 0;
        for (String s : str.split("\n")) {
            if (idx != 0) {
                res += "\n";
            }
            res += repeat(" ", level) + s;
            ++idx;
        }

        return res;
    }

    public static String fmt(String fmt, Object... args) {
        return String.format(fmt, args);
    }

    public static String repeat(String str, int times) {
        return new String(new char[times]).replace("\0", str);
    }
}
