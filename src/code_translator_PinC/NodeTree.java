package code_translator_PinC;


import static code_translator_PinC.StringUtils.*;

import code_translator_PinC.Lexeme;
import java.util.ArrayList;
import java.util.EnumSet;


public class NodeTree
{

    final nk kind;

    Lexeme startLex;

    ArrayList<NodeTree> subnodes;

    String str_value;
    int    int_value;
    double real_value;

    // public ArrayList<NodeTree> getSubnodes() { return subnodes; }

    public String getStr() {
        assert this.kind.in(nk.StrKinds);
        return str_value;
    }

    public int getInt() {
        assert this.kind == nk.Int;
        return int_value;
    }

    public double getReal() {
        assert this.kind == nk.Real;
        return real_value;
    }


    public NodeTree(nk kind) {
        this.kind = kind;
        if (kind.hasSubnodes()) {
            this.subnodes = new ArrayList<NodeTree>();
        }
        // System.out.println(kind.toString());
    }

    public NodeTree(Lexeme lex) {
        startLex = lex;
        switch (lex.get_type()) { // case lex_Number:
            case lex_Name: {
                kind      = nk.Ident;
                str_value = lex.get_value();
                break;
            }

            case lex_String: {
                kind      = nk.Str;
                str_value = lex.get_value();
                break;
            }

            case lex_Real: {
                kind       = nk.Real;
                real_value = Float.parseFloat(lex.get_value());
                break;
            }

            case lex_Number: {
                kind      = nk.Int;
                int_value = Integer.parseInt(lex.get_value());
                break;
            }

            case lex_bracket_R:
            case lex_bracket_L: {
                kind = nk.Par;
                break;
            }

            case lex_equal:
            case lex_more:
            case lex_less:
            case lex_more_equal:
            case lex_less_equal:
            case lex_not_equal:
            case lex_and:
            case lex_or: {
                kind      = nk.OpIdent;
                str_value = lex.get_value();
                break;
            }

            default: {
                kind = nk.Int;
                assert false : "fuck";
            }
        }

        if (kind.in(nk.StmtKinds)) {
            subnodes = new ArrayList<NodeTree>();
        }
    }


    public static NodeTree tree(String str_value) {
        NodeTree res  = new NodeTree(nk.Str);
        res.str_value = str_value;
        return res;
    }


    public static NodeTree tree(nk kind, String str_value) {
        NodeTree res  = new NodeTree(kind);
        res.str_value = str_value;
        return res;
    }

    public static NodeTree tree(int int_value) {
        NodeTree res  = new NodeTree(nk.Int);
        res.int_value = int_value;
        return res;
    }

    public static NodeTree tree(double real_value) {
        NodeTree res   = new NodeTree(nk.Real);
        res.real_value = real_value;
        return res;
    }

    public static NodeTree tree(nk kind, ArrayList subnodes) {
        kind.expectKind(nk.StmtKinds, nk.ExprKinds);
        NodeTree res = new NodeTree(kind);
        res.subnodes = subnodes;
        return res;
    }

    public static NodeTree tree(nk newKind, NodeTree... subnodes) {
        newKind.expectKind(nk.StmtKinds, nk.ExprKinds);

        NodeTree res = new NodeTree(newKind);

        for (NodeTree node : subnodes) {
            res.subnodes.add(node);
        }
        return res;
    }

    public String stringNode() {
        String str = kind.toString();

        switch (kind) {
            case Ident:
            case Infix:
            case OpIdent:
            case Str: {
                str += " " + str_value;
                break;
            }

            case Int: {
                str += " " + str + String.valueOf(int_value);
                break;
            }

            case Real: {
                str += " " + str + String.valueOf(real_value);
            }
        }

        return str;
    }

    public String treeRepr(int level) {
        String str = repeat("  ", level) + kind.toString();

        if (this.kind.in(nk.LiterKinds)) {
            return repeat("  ", level) + stringNode();
        } else {
            if (subnodes.size() > 0) {
                str += "\n";
            }

            for (int i = 0; i < subnodes.size(); ++i) {
                str += subnodes.get(i).treeRepr(level + 1);
                if (i < subnodes.size() - 1) {
                    str += "\n";
                }
            }

            return str;
        }
    }

    public String lispRepr() {
        if (this.kind.in(nk.LiterKinds)) {
            return "(" + stringNode() + ")";
        } else {
            String str = stringNode();
            if (subnodes.size() > 0) {
                str += " ";
            }


            for (int i = 0; i < subnodes.size(); ++i) {
                if (i > 0) {
                    str += " ";
                }

                str += subnodes.get(i).lispRepr();
            }

            return "(" + str + ")";
        }
    }

    public void add(ArrayList nodes) { this.subnodes.addAll(nodes); }
    public void add(NodeTree node) { this.subnodes.add(node); }
    public void add(Lexeme lex) { this.subnodes.add(new NodeTree(lex)); }
    public int  size() { return this.subnodes.size(); }


    public void add(nk newKind, String value) {
        this.subnodes.add(tree(newKind, value));
    }

    public NodeTree get(int idx) { return subnodes.get(idx); }

    public void add(nk newKind, NodeTree... subnodes) {
        this.subnodes.add(tree(newKind, subnodes));
    }

    public nk getKind() { return kind; }

    public ArrayList<NodeTree> getSubnodes() { return subnodes; }
}
