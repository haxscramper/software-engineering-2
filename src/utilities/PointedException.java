package utilities;

// Класс ошибки с позицией
public class PointedException extends Exception
{

    public String message;
    public int    line_position;
    public int    symbol_position;
    public int    line_position_end;
    public int    symbol_position_end;


    public PointedException(
        String message,
        int    line_position,
        int    symbol_position,
        int    line_position_end,
        int    symbol_position_end) {

        this.message             = message;
        this.line_position       = line_position;
        this.symbol_position     = symbol_position;
        this.line_position_end   = line_position;
        this.symbol_position_end = symbol_position;
    }
}
