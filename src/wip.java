import static code_translator_PinC.NodeTree.tree;

import code_translator_PinC.*;
import java.io.File;
import java.util.ArrayList;
import utilities.PointedException;

public class wip
{
    public static NodeTree parseString(String intext, boolean doLogging)
        throws PointedException {

        TextDriver driver = new TextDriver(intext);

        Scanner           scanner = new Scanner(driver);
        ArrayList<Lexeme> lexemes = scanner.analysis();
        Syntax            syntax  = new Syntax(lexemes, driver);
        syntax.toggleLogging(doLogging);
        NodeTree tree = syntax.analysis();

        return tree;
    }

    public static void parseTest(
        String  intext,
        boolean doLogging,
        boolean doPrint) throws PointedException {

        String tree = parseString(intext, doLogging).treeRepr(0);
        if (doPrint) {
            System.out.println(tree);
        }
    }

    public static void convertTest(String intext, boolean doLogging)
        throws PointedException {
        NodeTree tree = parseString(intext, doLogging);
        {
            CppConverter conv = new CppConverter();
            System.out.println(conv.convert(tree));
        }

        {
            JavaConverter conv = new JavaConverter();
            System.out.println(conv.convert(tree));
        }

        {
            PythonConverter conv = new PythonConverter();
            System.out.println(conv.convert(tree));
        }
    }

    public static void main(String[] args) {
        try {


            if (false) {
                parseTest("begin a := 100; end.", false, false);
                convertTest("begin a := 100; end.", false);

                parseTest("begin a := 100; b := a; end.", false, false);

                parseTest("begin if a then b := 1; end.", false, true);
                convertTest("begin if a then b := 1; end.", false);

                parseTest(
                    "begin\n"
                        + "  a := 12 - 2;\n"
                        + "  while b == 10 do\n",
                    false,
                    true);
            }

            if (false) {
                String s1 = "\n"
                            + "begin\n"
                            + "  a := 12 - 2;\n"
                            + "  while b = 10 do\n"
                            + "  begin\n"
                            + "    b := rand(a);\n"
                            + "  end;\n"
                            + "end.";

                parseTest(s1, false, true);
                convertTest(s1, false);
            }

            if (true) {
                String intext = //
                    "function max(num1, num2: integer): integer;\n"
                    + "\n"
                    + "var\n"
                    + "   result: integer;\n"
                    + "\n"
                    + "begin\n"
                    + "   if true then\n"
                    + "      result := num1;\n"
                    + "   else\n"
                    + "      result := num2;\n"
                    + "   max := result;\n"
                    + "end;\n"
                    + "\n"
                    + "begin\n"
                    + "  a := 100;\n"
                    + "end.\n";


                try {
                    parseTest(intext, false, true);
                } catch (PointedException err) {
                    int lineIdx = 1;
                    for (String line : intext.split("\n")) {
                        if (err.line_position <= lineIdx
                            && lineIdx <= err.line_position_end) {
                            for (int i = 0; i < line.length(); ++i) {
                                char c = line.charAt(i);
                                if (err.symbol_position <= (i + 1)
                                    && (i + 1) <= err.symbol_position_end + 5) {
                                    System.out.printf("\033[31m%s\033[39m", c);
                                } else {
                                    System.out.printf("%s", c);
                                }
                            }
                            System.out.printf("\n");

                        } else if (
                            err.line_position - 4 <= lineIdx
                            && lineIdx <= err.line_position_end) {
                            System.out.printf("%s\n", line);
                        }
                        ++lineIdx;
                    }
                    System.out.printf("\n%s\n", err.message);
                }

                // convertTest(intext, false);
            }

        } catch (PointedException error) {
            error.printStackTrace();
            System.out.println(error.message);
        }
    }
}
